import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Registration from '@/components/Registration'
import SurveyList from '@/components/SurveyList'
import SurveyCreate from '@/components/SurveyCreate'
import SurveyInfo from '@/components/SurveyInfo'
import SurveyEdit from '@/components/SurveyEdit'
import SurveyPass from '@/components/SurveyPass'
import SurveyStatistics from '@/components/SurveyStatistics'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: '',
      component: Login
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: Registration
    },
    {
      path: '/surveys',
      name: 'surveyList',
      component: SurveyList
    },
    {
      path: '/surveys/create',
      name: 'surveyCreate',
      component: SurveyCreate
    },
    {
      path: '/surveys/:surveyId/info',
      name: 'surveyInfo',
      component: SurveyInfo
    },
    {
      path: '/surveys/:surveyId/',
      name: 'surveyPass',
      component: SurveyPass
    },
    {
      path: '/surveys/:surveyId/edit',
      name: 'surveyEdit',
      component: SurveyEdit
    },
    {
      path: '/surveys/:surveyId/statistics',
      name: 'surveyStatistics',
      component: SurveyStatistics
    }
  ]
})

export default router
